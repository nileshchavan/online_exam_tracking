package com.service.examination;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bean.examination.SetExamTimeTable;
import com.dao.examination.ExamTimeTableData;
import com.util.student.DateValidation;
import com.view.examination.DisplayExamTimeTable;


public class SetExamTimeTableImpl {

	// creating the  list for SetExamtimetable
		static List<SetExamTimeTable> list = new ArrayList<SetExamTimeTable>();
		/**
		 * This method is used to set the timetable
		 * @param filePath
		 * @throws IOException
		 */
		public static void SetMidTerm1TimeTable(String filePath) throws IOException {
			Scanner sc = new Scanner(System.in);	
			list = ExamTimeTableData.getExamTimeTableList(filePath);
			if(list.size() > 0) {
				System.out.println("Time Table is already set, do you want to re-set (yes/no)");
				String choice = sc.next();
				if(choice.equalsIgnoreCase("yes")) {
					list.clear();
					updateMidTerm1TimeTable(sc, filePath );
				} else {
					DisplayExamTimeTable.displayTimeTable(filePath , "First Term");
				}
			} else {
				updateMidTerm1TimeTable(sc, filePath);
			}
		}
		/**
		 * this method is used to update the timetable
		 * @param sc
		 * @param filePath
		 */
		public static void updateMidTerm1TimeTable(Scanner sc , String filePath) {
			ArrayList<SetExamTimeTable> midTerm1List = new ArrayList<SetExamTimeTable>();
			System.out.println("How many subject you want add");
			int subSize = 0;
			try {
				subSize = sc.nextInt();
			} catch(Exception e) {
				System.out.println("Please Enter Integers only");
				System.exit(0);
			}
			
			int choice=2;
			
			for(int i = 0; i < subSize; i++) {
				SetExamTimeTable setMid1 = new SetExamTimeTable();
				System.out.println("Enter Date");
				DateValidation dateValid=new DateValidation();
				 String date=dateValid.isThisDateValid(choice);
				 setMid1.setDate(date);
				System.out.println("Enter Subject");
				String subject = sc.next();
				setMid1.setSubject(subject);
				
				System.out.println("Enter Time");
				String time = sc.next();
				setMid1.setTime(time);
				
				System.out.println("Enter Venue");
				String venue = sc.next();
				setMid1.setVenue(venue);
				
				midTerm1List.add(setMid1);
			}
			System.out.println("Added successfully");
			list.addAll(midTerm1List);
			try {
				ExamTimeTableData.saveExamTimeTableList(list,filePath);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
}
