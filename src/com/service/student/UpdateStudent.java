package com.service.student;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.bean.student.StudentDetails;
import com.controller.student.RegisterStudent;
import com.dao.student.StudentData;
import com.util.student.MobileEmailValidation;


public class UpdateStudent {

	static List<StudentDetails> list = new ArrayList<StudentDetails>();
	/**
	 * This method first check whether particular student record present or not if
	 * present in it will call getStudentForUpadte() method else it will display
	 * student record not found
	 * 
	 * @throws IOException
	 */
	public static boolean checkStudentForUpdate() throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		try {
			list = StudentData.getStudentList();
			Iterator<StudentDetails> it = list.iterator();
			System.out.println("Enter Student id to update");
			long id=0;
			try {
			 id = sc.nextLong();
			}catch(Exception e) {
				System.err.print("please enter integer only");
			}
			while (it.hasNext()) {
				StudentDetails st1 = (StudentDetails) it.next();
				if (st1.getStudent_id() == id) {
					getStudentForUpdate(st1, list, sc);
					flag = true;
					break;
				}
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * if student record found it will perform update operation on some of field
	 * 
	 * @param
	 */
	public static void getStudentForUpdate(StudentDetails student, List<StudentDetails> list, Scanner sc) {

		new RegisterStudent().updateStudentById(student, list);

	}
}
