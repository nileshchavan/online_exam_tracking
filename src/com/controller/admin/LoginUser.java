package com.controller.admin;

import java.io.IOException;
import java.util.Scanner;
import com.service.admin.AdminLoginDetailsImpl;
import com.service.student.StudentLogins;
/**
 * class containing main method making admin or student to login into their details
 * @author BATCH-'C'
 *
 */
public class LoginUser {
	//variable count is for not allowing user to enter wrong user-id and password
	static int count=0;
	/**
	 * main method for selecting user and making user to login
	 * @param args
	 * @throws IOException
	 */
	public void loginUser() {
		Scanner sc = new Scanner(System.in);
		System.out.println("============================");
		System.out.println("Select User");
		System.out.println("============================");
		AdminLoginDetailsImpl adminLoginDetails = new AdminLoginDetailsImpl();
		System.out.println("1) Admin");
		System.out.println("2) Student");
		System.out.println("-----------------------------");
		int choice=0;
		try {
		choice= Integer.parseInt(sc.nextLine());
		while(choice<=0||choice>2) {
			System.err.println("please select valid option");
			choice= sc.nextInt();
		}
		}
		catch(Exception e) {
			System.err.println("please enter integers only");
			loginUser();
		}
	
		switch(choice) {
		case  1 : {
			try {
				// calling Admin log in method 
				adminLoginDetails.setAdminDetails();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			break;
		}
		case 2 : {
               //calling  User log in method
			try {
				StudentLogins studentLoginDetails1=new StudentLogins();
				studentLoginDetails1.studentLoginDetails();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			break;
		}
		default : {
			System.out.println("You have Entered wrong choice");
		}
	}//end of switch
	
	//Scanner Close
//	sc.close();
	}
}
