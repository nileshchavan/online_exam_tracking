package com.controller.examination;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.controller.admin.AdminMenu;
import com.service.examination.SetExamTimeTableImpl;
import com.view.examination.DisplayExamTimeTable;


/**
 * This class Displays Exam Functionality
 * 
 * @author BATCH-'C'
 *
 */
public class ExamMenu {

	public static void getExaminationMenu() throws FileNotFoundException {

		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			// Displaying Menu to Admin
			System.out.println("==============================================");
			System.out.println("\nWelcome to Examination portal");
			System.out.println("==============================================");
			System.out.println("Select the Branch");
			System.out.println("----------------------------------------------");
			System.out.println("1) MPC ");
			System.out.println("2) BPC");
			System.out.println("3) Go Back");
			System.out.println("4) Go Back To Admin Menu");
			System.out.println("----------------------------------------------");
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {

				System.out.println(" please enter the valid choice");
				getExaminationMenu();
			}
			switch (choice) {
			case 1: {
				MPCExamMenu(choice);
				break;
			}
			case 2: {
				MPCExamMenu(choice);
				break;
			}
			case 3: {
				MasterExamMenu.getMasterMenuForExam();
				break;
			}
			case 4: {
				AdminMenu menu = new AdminMenu();
				try {
					menu.adminFunctions();
				} catch (IOException e) {

					e.printStackTrace();
				}
				break;
			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));
	}

	/**
	 * the class is to used to display the time table for students
	 * 
	 * @param branchChoice pareameter to choose thye branch
	 * @throws FileNotFoundException
	 */
	public static void MPCExamMenu(int branchChoice) throws FileNotFoundException {
		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			// Displaying Menu to Admin
			System.out.println("==============================================");
			System.out.println("\nWelcome to Examination portal");
			System.out.println("==============================================");
			System.out.println("Select the Exam Type");
			System.out.println("----------------------------------------------");
			System.out.println("1) First Mid Term  ");
			System.out.println("2) Second Mid Term");
			System.out.println("3) Final Exam");
			System.out.println("4) View All Time Table");
			System.out.println("5) Go Back");
			System.out.println("----------------------------------------------");
			
			int choice=0;
			try {
				choice = sc.nextInt();
			}
			catch(Exception e)
			{
				System.out.println("enter the valid choice");
				MPCExamMenu(branchChoice);
			}
			switch (choice) {
			case 1: {
				// after selection type of exam this method set exam time table
				try {
					String filePath = null;
					if (branchChoice == 1) {
						filePath = "ExamTrackingDataFiles\\MPCmid1timetable";
					} else {
						filePath = "ExamTrackingDataFiles\\BPCmid1timetable";
					}
					SetExamTimeTableImpl.SetMidTerm1TimeTable(filePath);
				} catch (IOException e) {

					e.printStackTrace();
				}
				break;
			}
			case 2: {
				try {
					String filePath = null;
					if (branchChoice == 1) {
						filePath = "ExamTrackingDataFiles\\MPCmid2timetable";
					} else {
						filePath = "ExamTrackingDataFiles\\BPCmid2timetable";
					}
					SetExamTimeTableImpl.SetMidTerm1TimeTable(filePath);
					// MPCMidTermExam2.SetMidTerm2TimeTable();
				} catch (IOException e) {

					e.printStackTrace();
				}

				break;
			}
			case 3: {
				try {
					String filePath = null;
					if (branchChoice == 1) {
						filePath = "ExamTrackingDataFiles\\MPCFinaltimetable";
					} else {
						filePath = "ExamTrackingDataFiles\\BPCFinaltimetable";
					}
					SetExamTimeTableImpl.SetMidTerm1TimeTable(filePath);
					// MPCFinalExam.SetMPCFinalTimeTable();
				} catch (IOException e) {

					e.printStackTrace();
				}
				break;
			}
			case 4: {
				try {
					if (branchChoice == 1) {
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCmid1timetable", "First Term");
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCmid2timetable", "Second Term");
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCFinaltimetable", "Final");
					} else {
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\BPCmid1timetable", "First Term");
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\BPCmid2timetable", "Second Term");
						DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\BPCFinaltimetable", "Final");
					}
					// MidTermExam1.displayTimeTable();
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				break;
			}
			case 5: {
				getExaminationMenu();
				break;
			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));
	}
}
