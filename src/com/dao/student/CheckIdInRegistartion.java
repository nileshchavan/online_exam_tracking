package com.dao.student;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bean.student.StudentDetails;
public class CheckIdInRegistartion {
	
		/***
		 * This method get Student Id from User(Admin) and checks whether 
		 * present or not , return true if present if not return
		 *@return true if present false if not present
		 *@throws FileNotFoundException
		 */
		@SuppressWarnings("unchecked")
		/**
		 * This method is to checkid if student is present sign up for the student 
		 * @param std_id
		 * @return true if student is available
		 * @throws FileNotFoundException
		 */
		public static boolean CheckId(long std_id) throws FileNotFoundException{
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
			
			List<StudentDetails> list2 = new ArrayList<StudentDetails>();
			
			try{
				@SuppressWarnings("resource")
				ObjectInputStream oi = new ObjectInputStream(fi);
				list2 = (List<StudentDetails>) oi.readObject();
				@SuppressWarnings("rawtypes")
				Iterator itr=list2.iterator();
				while(itr.hasNext()) {
					StudentDetails std=(StudentDetails) itr.next();
					if(std.getStudent_id()==std_id) {
						return true;
					}
				}
			}catch(Exception e){
			   System.out.println();
			}
			return false;
			
		}
	}

