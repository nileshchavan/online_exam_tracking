package com.dao.examination;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.bean.examination.SetExamTimeTable;


public class ExamTimeTableData {
	
	@SuppressWarnings("unchecked")
	/**
	 * This method is used to get the listobjects  timetable from the file
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<SetExamTimeTable> getExamTimeTableList(String filePath) throws FileNotFoundException {
		FileInputStream fi =null; 
		
		List<SetExamTimeTable> list2 = new ArrayList<SetExamTimeTable>();
		
		try{
			fi=new FileInputStream(new File(filePath));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetExamTimeTable>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		return list2;
	}
	/**
	 * This method is used to write the listobjects to the file
	 * @param list
	 * @param filePath
	 * @throws IOException
	 */
	public static void saveExamTimeTableList(List<SetExamTimeTable> list , String filePath) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		obj.writeObject(list);
	}
}
