package com.dao.examination;
import com.bean.student.AcademicInfo;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
public class EligibleCheckDetails {

	// method for setting academic details into a new file
	public static void setAcademicRecord(List<AcademicInfo> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\EligibleCheck.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}

		obj.writeObject(list);

	}

	// method for getting StudentInfo list from file
	@SuppressWarnings("unchecked")
	public static List<AcademicInfo> getAcademicList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\EligibleCheck.txt"));

		List<AcademicInfo> list2 = new ArrayList<AcademicInfo>();

		try {
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<AcademicInfo>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}

		return list2;
	}


}
