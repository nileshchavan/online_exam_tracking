package com.admin.service;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.admin.service.SetLoginDetails;
import com.admin.service.SignupStudents;
import com.student.service.StudentMenu;
/**
 *  this class used for login into student page
 * @author BATCH-C
 *
 */
public class StudentLoginDetails {
	static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	static int count = 0;

	 @SuppressWarnings("unused")
	 /**
	  * This  method to check the student credentials whether it is correct or not 
	  * @throws IOException
	  */
	public void studentLoginDetails() throws IOException {
		Scanner sc = new Scanner(System.in);
		SignupStudents signupStudents = new SignupStudents();
		list = SignupStudents.getLoginDetails();
		
		// Validation checking
		System.out.println("enter the user name");
		String userName = sc.nextLine();
		System.out.println("enter the password");
		String password = sc.nextLine();
		Iterator<SetLoginDetails> itr = list.iterator();

		// checking the credentials
		while (itr.hasNext()) {
			SetLoginDetails admin1 = (SetLoginDetails) itr.next();
			if (userName.equalsIgnoreCase(admin1.getUsername()) && password.equalsIgnoreCase(admin1.getPassword())) {
				System.out.println("success");
				StudentMenu menu = new StudentMenu();
				menu.studentMenu();
				System.exit(0);
			} else {
				count++;
			}
		}

		// Object creation, if user not entered correct credentials
		SetLoginDetails admin2 = new SetLoginDetails();

		if (!(userName.equalsIgnoreCase(admin2.getUsername())) || !(password.equalsIgnoreCase(admin2.getPassword())))
			if (count <= 100) {
				for (int i = 0; i < 3; i++) {
					System.out.println("please enter the correct details");
					studentLoginDetails();
					break;
				}
                Iterator<SetLoginDetails> itr2 = list.iterator();
				while (itr2.hasNext()) {
					SetLoginDetails admin3 = (SetLoginDetails) itr2.next();
					for (int j = 0; j < 3; j++) {
						System.out.println("please enter the correct user name to get the credentials");
						userName = sc.nextLine();
						System.out.println("please answer the security question");
						System.out.println("please enter your favorite color");
						String securityQuestion = sc.nextLine();
						if (userName.equalsIgnoreCase(admin3.getUsername())
								&& securityQuestion.equalsIgnoreCase(admin3.getSecurityQues())) {
							System.out.println("Your username is : " + admin3.getUsername());
							System.out.println("Your password is : " + admin3.getPassword());
							studentLoginDetails();
							break;
						}
					}
					if (!(userName.equalsIgnoreCase(admin2.getUsername()))
							|| !(password.equalsIgnoreCase(admin2.getPassword()))) {
						System.out.println("Too many attempts!!! please try again after sometime");
						System.exit(0);
					}
				}
			}
		sc.close();
	}
        /**
		 * This method return List Object Stored in file
		 * 
		 * @throws  IOException
		 */
	public static void saveAdminRecord(List<SetLoginDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\StudentDetails.txt"));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
		
			e.printStackTrace();
		}

		obj.writeObject(list);

	}
	/**
	 * This method recently added or updated record in file
	 * 
	 * @throws FilenotFound
	 */
	@SuppressWarnings("unchecked")
	public static List<SetLoginDetails> getAdminList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\StudentDetails.txt"));

		List<SetLoginDetails> list2 = new ArrayList<SetLoginDetails>();

		try {
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetLoginDetails>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}

		return list2;
	}
}
