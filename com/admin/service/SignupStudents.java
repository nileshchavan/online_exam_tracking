package com.admin.service;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.StudentRegistration.StudentDetails;
import com.fieldvalidation.MobileEmailValidation;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
public class SignupStudents  {
	 static List<SetLoginDetails> list1 = new ArrayList<SetLoginDetails>();
	 static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	 /**
	 * @throws IOException 
	 *  This method used to create student login credentials
	 */
	public void signupNewStudent() throws IOException {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		//getting list from file
		 list = getLoginDetails();
		 
		
		//getting id for setting password to student
		System.out.println("enter the student id to set the password");
		long stud_id=sc.nextLong();
		boolean checkid=CheckId(stud_id);
		boolean checkid1=CheckId1(stud_id);
		if(list.size()>0){
			if(checkid1)
			{
			System.out.println("student details already entered");	
		    }
			else {
				//checking whether student present or not
				if(checkid) {	
				SetLoginDetails student = new SetLoginDetails();
				
				//if student present , then setting new credentials to student
				System.out.println("Enter the username");
				String userName = sc.next();
				student.setUsername(userName);
				// validating the password for student
				MobileEmailValidation validate = new MobileEmailValidation();
				System.out.println("Enter the password");
				String password = sc.next();
				boolean flag = true;
				do {
					if(validate.validatePassword(password)) {
						student.setPassword(password);
						flag = false;
						break;
					} else {
						System.out.println("Password must contains following Characters");
						System.out.println("One lowercase , one uppercase , one special symbol , one number and length between 6 to 20");
						password = sc.next();
					}
				}while(flag);
				student.setPassword(password);

				String securityQues = "***";
				student.setSecurityQues(securityQues);
				student.setStud_id(stud_id);
				list.add(student);
				saveStudentRecord(list);
				}
				else {
					System.out.println("Id not found,please enter the correct student  id for signup");
					signupNewStudent();
				}    
			
			}
		}
		else {
			System.out.println("list is empty");
		}
}
	/***
	 * This method get Student Id from User(Admin) and checks whether 
	 * present or not , return true if present if not return
	 *@return true if present false if not present
	 *@throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	/**
	 * This method is to checkid if student is present sign up for the student 
	 * @param std_id
	 * @return true if student is available
	 * @throws FileNotFoundException
	 */
	public static boolean CheckId(long std_id) throws FileNotFoundException{
	FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
		
		List<StudentDetails> list2 = new ArrayList<StudentDetails>();
		
		try{
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<StudentDetails>) oi.readObject();
			@SuppressWarnings("rawtypes")
			Iterator itr=list2.iterator();
			while(itr.hasNext()) {
				StudentDetails std=(StudentDetails) itr.next();
				if(std.getStudent_id()==std_id) {
					return true;
				}
			}
		}catch(Exception e){
		   System.out.println();
		}
		return false;
		
	}
	/***
	 * This method get Student Id from User(Admin) and checks whether 
	 * present or not , return true if present if not return
	 *@return true if present false if not present
	 *@throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	/**
	 * This method is to checkid if student is present sign up for the student 
	 * @param std_id
	 * @return true if student is available
	 * @throws FileNotFoundException
	 */
	public static boolean CheckId1(long std_id) throws FileNotFoundException{
	FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\StudentLogins"));
		
		List<SetLoginDetails> list2 = new ArrayList<SetLoginDetails>();
		
		try{
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetLoginDetails>) oi.readObject();
			@SuppressWarnings("rawtypes")
			Iterator itr=list2.iterator();
			while(itr.hasNext()) {
				SetLoginDetails std=(SetLoginDetails) itr.next();
				if(std.getStud_id()==std_id) {
					return true;
				}
			}
		}catch(Exception e){
		   System.out.println();
		}
		return false;
		
	}
	@SuppressWarnings("unchecked")
	/**
	 * This method fetching student login credentials from the file
	 * @return
	 */
	public static List<SetLoginDetails> getLoginDetails(){
		FileInputStream fi =null; 
		
		List<SetLoginDetails> list2 = new ArrayList<SetLoginDetails>();
		
		try{
			fi=new FileInputStream(new File("ExamTrackingDataFiles\\StudentLogins"));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetLoginDetails>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		return list2;
	}
	/**
	 * This method to adding and updating  listobjects into the file
	 * @param list
	 * @throws IOException
	 */
	public static void saveStudentRecord(List<SetLoginDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\StudentLogins"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		obj.writeObject(list);
		
		System.out.println("Saved");
	}
	/**
	 * This method is used to display the studentDetails
	 *   
	 */
	public static void printLoginAllRecords() throws FileNotFoundException {
		list= getLoginDetails();
		if(list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<SetLoginDetails> it = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%5s %28s %28s %28s", "Student ID", "user name","password","secuirity question");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	SetLoginDetails st1 = (SetLoginDetails)it.next();
	            System.out.format("%5s %30s %30s %30s",st1.getStud_id(),st1.getUsername(),st1.getPassword(),st1.getSecurityQues());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
		
	}
	
	
	}


