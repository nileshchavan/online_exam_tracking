package com.admin.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.admin.service.AdminMenu;

/**
 *  @author Batch-'C'
 * This class is for admin hardcoded values
 * 
 *
 *
 */
public class Loginserialization {
	// list to set and get the admin login details from file
	static List<AdminLoginDetails> list = new ArrayList<AdminLoginDetails>();
	static int count = 0;

	/**
	 * This method is for setting hard-coded values for admin
	 * 
	 * @throws IOException
	 */
	public void adminLoginDetails() throws IOException {
		Scanner sc = new Scanner(System.in);

		AdminLoginDetails adminLogin = new AdminLoginDetails();
		adminLogin.setUserName("varun");
		adminLogin.setPassword("varun123");
		adminLogin.setSecurityQues("black");
		list.add(adminLogin);

		AdminLoginDetails adminLogin1 = new AdminLoginDetails();
		adminLogin1.setUserName("nilesh");
		adminLogin1.setPassword("nilesh123");
		adminLogin1.setSecurityQues("maroon");
		list.add(adminLogin1);

		AdminLoginDetails adminLogin2 = new AdminLoginDetails();
		adminLogin2.setUserName("megha");
		adminLogin2.setPassword("megha123");
		adminLogin2.setSecurityQues("blue");
		list.add(adminLogin2);

		AdminLoginDetails adminLogin3 = new AdminLoginDetails();
		adminLogin3.setUserName("aravind");
		adminLogin3.setPassword("aravind123");
		adminLogin3.setSecurityQues("white");
		list.add(adminLogin3);

		AdminLoginDetails adminLogin4 = new AdminLoginDetails();
		adminLogin4.setUserName("vineehsa");
		adminLogin4.setPassword("vineesha123");
		adminLogin4.setSecurityQues("green");
		list.add(adminLogin4);

		saveAdminRecord(list);
		// getting list from file
		list = getAdminList();

		// Validation checking
		System.out.println("enter the user name");
		String userName = sc.nextLine();
		System.out.println("enter the password");
		String password = sc.nextLine();

		Iterator<AdminLoginDetails> itr = list.iterator();

		// checking the credentials

		while (itr.hasNext()) {
			AdminLoginDetails admin1 = (AdminLoginDetails) itr.next();
			if (userName.equalsIgnoreCase(admin1.getUserName()) && password.equalsIgnoreCase(admin1.getPassword())) {
				System.out.println("success");
				//Creating Admin object for performing admin functions
				
				AdminMenu menu = new AdminMenu();
				menu.adminFunctions();
				System.exit(0);
			} else {
				count++;
			}
		}
		// Object creation, if user not entered correct credentials
		AdminLoginDetails admin2 = new AdminLoginDetails();

		if (!(userName.equalsIgnoreCase(admin2.getUserName())) || !(password.equalsIgnoreCase(admin2.getPassword())))
			if (count <= 6) {
				for (int i = 0; i < 3; i++) {
					System.out.println("please enter the correct details");
					adminLoginDetails();
				}

				Iterator<AdminLoginDetails> itr2 = list.iterator();
				
				while (itr2.hasNext()) {
					AdminLoginDetails admin3 = (AdminLoginDetails) itr2.next();
					
					for (int j = 0; j < 3; j++) {
						System.out.println("please enter the correct user name to get the credentials");
						userName = sc.nextLine();
						
						System.out.println("please answer the security question");
						System.out.println("please enter your favorite color");
						
						String securityQuestion = sc.nextLine();
						
						if (userName.equalsIgnoreCase(admin3.getUserName())
								&& securityQuestion.equalsIgnoreCase(admin3.getSecurityQues())) {
							System.out.println("Your username is : " + admin3.getUserName());
							System.out.println("Your password is : " + admin3.getPassword());
							adminLoginDetails();
						}
					}
					
					if (!(userName.equalsIgnoreCase(admin2.getUserName()))
							|| !(password.equalsIgnoreCase(admin2.getPassword()))) {
						System.out.println("Too many attempts!!! please try again after sometime");
						System.exit(0);
					}
				}
			}
		sc.close();
	}

	/**
	 * This method  written the list objects  in the file
	 * 
	 * @throws IOException
	 */

	public static void saveAdminRecord(List<AdminLoginDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\AdminDetails.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}

		obj.writeObject(list);

	}

	/**
	 * This method recently added or updated record in file
	 * 
	 * @throws FilenotFound
	 */
	@SuppressWarnings("unchecked")
	/**
	 * This method is used to read the list objects from  the file
	 */
	public static List<AdminLoginDetails> getAdminList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\AdminDetails.txt"));

		List<AdminLoginDetails> list2 = new ArrayList<AdminLoginDetails>();
		ObjectInputStream oi = null;
		try {
			oi = new ObjectInputStream(fi);
			list2 = (List<AdminLoginDetails>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}
		try {
			oi.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list2;
	}
}
