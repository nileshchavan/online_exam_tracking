package com.StudentRegistration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.fieldvalidation.MobileEmailValidation;

public class UpadateStudent {

	static List<StudentDetails> list = new ArrayList<StudentDetails>();

	/**
	 * This method first check whether particular student record present or not if
	 * present in it will call getStudentForUpadte() method else it will display
	 * student record not found
	 * 
	 * @throws IOException
	 */
	public static boolean checkStudentForUpdate() throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		try {
			list = StudentDetailsImpl.getStudentList();
			Iterator<StudentDetails> it = list.iterator();
			System.out.println("Enter Student id to update");
			long id = sc.nextLong();
			while (it.hasNext()) {
				StudentDetails st1 = (StudentDetails) it.next();
				if (st1.getStudent_id() == id) {
					getStudentForUpdate(st1, list, sc);
					flag = true;
					break;
				}
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * if student record found it will perform update operation on some of field
	 * 
	 * @param
	 */
	public static void getStudentForUpdate(StudentDetails student, List<StudentDetails> list, Scanner sc) {

		MobileEmailValidation validate = new MobileEmailValidation();
		System.out.println("select operation");
		System.out.println("1) Update mobile number");
		System.out.println("2) update parent mobile number");
		System.out.println("3) update email id");
		System.out.println("4) update address");
		int choice = 0;
		try {
			choice = sc.nextInt();
			while (choice <= 0 || choice > 4) {
				choice = sc.nextInt();
			}
		} catch (Exception e) {
			System.err.println("please enter integers only");
		}
		if (choice == 1) {
			System.out.println("Enter Updated mobile Number");
			String mobile_no = sc.next();
			boolean flag1 = true;
			do {
				if (validate.isValidMobileNumber(mobile_no)) {
					student.setMobile_no(mobile_no);
					flag1 = false;
					break;
				} else {
					System.out.println("Please Enter Correct Mobile Number");
					mobile_no = sc.next();
				}
			} while (flag1);
		}
		if (choice == 2) {
			System.out.println("Enter Updated parent Number");
			String parent_no = sc.next();
			boolean flag2 = true;
			do {
				if (validate.isValidMobileNumber(parent_no)) {
					student.setParent_no(parent_no);
					flag2 = false;
					break;
				} else {
					System.out.println("Please Enter Correct Mobile Number");
					parent_no = sc.next();
				}
			} while (flag2);
		}
		if (choice == 3) {
			System.out.println("Enter Update Email Id");
			String email_id = sc.next();
			boolean flag = true;
			do {
				if (validate.isValidEmailId(email_id)) {
					student.setEmail_id(email_id);
					flag = false;
					break;
				} else {
					System.out.println("Please Enter valid Email Address");
					email_id = sc.next();
				}
			} while (flag);
		}
		if (choice == 4) {
			System.out.println("Enter New address");
			String address = sc.next();
			student.setAddress(address);
		}

		try {
			StudentDetailsImpl.saveStudentRecord(list);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			list = StudentDetailsImpl.getStudentList();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(student.getStudent_id() + " Updated Successfully.");

	}

}
